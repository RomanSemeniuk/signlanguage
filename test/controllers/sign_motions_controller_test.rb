require 'test_helper'

class SignMotionsControllerTest < ActionController::TestCase
  setup do
    @sign_motion = sign_motions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sign_motions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sign_motion" do
    assert_difference('SignMotion.count') do
      post :create, sign_motion: {  }
    end

    assert_redirected_to sign_motion_path(assigns(:sign_motion))
  end

  test "should show sign_motion" do
    get :show, id: @sign_motion
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sign_motion
    assert_response :success
  end

  test "should update sign_motion" do
    patch :update, id: @sign_motion, sign_motion: {  }
    assert_redirected_to sign_motion_path(assigns(:sign_motion))
  end

  test "should destroy sign_motion" do
    assert_difference('SignMotion.count', -1) do
      delete :destroy, id: @sign_motion
    end

    assert_redirected_to sign_motions_path
  end
end
