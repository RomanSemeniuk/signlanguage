json.extract! sign_motion, :id, :created_at, :updated_at
json.url sign_motion_url(sign_motion, format: :json)