class SignMotionsController < ApplicationController
  before_action :set_sign_motion, only: [:show, :edit, :update, :destroy]

  # GET /sign_motions
  # GET /sign_motions.json
  def index
    @sign_motions = SignMotion.all
  end

  # GET /sign_motions/1
  # GET /sign_motions/1.json
  def show
  end

  # GET /sign_motions/new
  def new
    @sign_motion = SignMotion.new
  end

  # GET /sign_motions/1/edit
  def edit
  end

  # POST /sign_motions
  # POST /sign_motions.json
  def create
    @sign_motion = SignMotion.new(sign_motion_params)

    respond_to do |format|
      if @sign_motion.save
        format.html { redirect_to @sign_motion, notice: 'Sign motion was successfully created.' }
        format.json { render :show, status: :created, location: @sign_motion }
      else
        format.html { render :new }
        format.json { render json: @sign_motion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sign_motions/1
  # PATCH/PUT /sign_motions/1.json
  def update
    respond_to do |format|
      if @sign_motion.update(sign_motion_params)
        format.html { redirect_to @sign_motion, notice: 'Sign motion was successfully updated.' }
        format.json { render :show, status: :ok, location: @sign_motion }
      else
        format.html { render :edit }
        format.json { render json: @sign_motion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sign_motions/1
  # DELETE /sign_motions/1.json
  def destroy
    @sign_motion.destroy
    respond_to do |format|
      format.html { redirect_to sign_motions_url, notice: 'Sign motion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sign_motion
      @sign_motion = SignMotion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sign_motion_params
      params.fetch(:sign_motion, {})
    end
end
